# Yahoo Finance Backtesting

Code for backtesting Yahoo finance historical data.

`montecarlo.py` will run a Monte Carlo simulation over an arbitary number of history files passed as cli arguments. For example `python montecarlo.py data/S\&P500.csv data/Nikkei\ 225.csv`

`backtest.py` will run a straight backtest over the historial data. Arguments are the same as montecarlo.py and the starting date can be changed in code.