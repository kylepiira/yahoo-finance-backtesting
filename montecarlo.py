import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import random
import sys

SIMULATIONS = 100
TRIALS = 10000
STARTING_BALANCE = 1
PER_TRIAL_DEPOSIT = 1

if __name__ == '__main__':
	# Get the history file
	try:
		filenames = sys.argv[1:]
	except IndexError:
		print('No history files')
		exit(0)
	for filename in filenames:
		print(filename)
		# Read the data
		df = pd.read_csv(filename)
		# Drop any NaN rows
		df.dropna(inplace=True)
		# Get the percent returns
		returns = list(df['Adj Close'].pct_change().dropna())
		histories = []
		for simulation in tqdm(range(SIMULATIONS)):
			history = []
			balance = STARTING_BALANCE
			for trial in range(TRIALS):
				choice = random.choice(returns)
				history.append(balance)
				balance *= 1 + choice
				balance += PER_TRIAL_DEPOSIT
			histories.append(history)

		histories = np.array(histories).mean(axis=0)
		plt.plot(histories, label=filename)

	plt.legend()
	plt.show()
