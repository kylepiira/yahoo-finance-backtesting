import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
from datetime import datetime
import random
import sys

STARTING_BALANCE = 1
PER_TRIAL_DEPOSIT = 1
START_DATE = datetime(1970, 1, 1)
END_DATE = datetime.now()

if __name__ == '__main__':
	# Get the history file
	try:
		filenames = sys.argv[1:]
	except IndexError:
		print('No history files')
		exit(0)
	for filename in filenames:
		print(filename)
		# Read the data
		df = pd.read_csv(filename)
		# Convert dates
		df['Date'] = pd.to_datetime(df['Date'])
		# Filter unwanted dates
		df = df[START_DATE < df['Date']]
		df = df[df['Date'] < END_DATE]
		# Drop any NaN rows
		df.dropna(inplace=True)
		# Get the percent returns
		returns = list(df['Adj Close'].pct_change().dropna())
		history = []
		dates = []
		balance = STARTING_BALANCE
		for change, date in zip(returns, df['Date']):
			choice = random.choice(returns)
			history.append(balance)
			dates.append(date)
			balance *= 1 + change
			balance += PER_TRIAL_DEPOSIT

		plt.plot(dates, history, label=filename)

	plt.legend()
	plt.show()
